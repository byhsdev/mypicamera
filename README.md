MyPICamera take a photo when someone mention it in twitter.
Also, get temp and humidity values and post a new status in twitter.

Twitter account: [@MyPICamera](http://twitter.com/MyPICamera)

The project has 4 files:

## mypicamera.py :
This is the main file. It uses the Twitter Stream API, and when someone mention @MyPICamera, this reply a status with a photo and temp/hum data.
    
## camera.py :
It uses the python-picamera library in order to take photos from the Raspberry PI Camera: http://picamera.readthedocs.org/en/release-1.5/
    
## environment.py :
This file get temp and humidity data from Z-Way API. The server is installed on same Raspberry PI and It's connected with external wireless sensors.
    http://razberry.z-wave.me/
    
## settings.py :
This file contains the Twitter API Keys and looks like this:

    CONSUMER_KEY = 'Your-consumer-key'
    CONSUMER_SECRET = 'Your-consumer-secret'
    ACCESS_TOKEN = 'Your-access-token'
    ACCESS_TOKEN_SECRET = 'Your access-token-secret'
    
#### Developer:
Antonio Sanchez Pineda

#### Contact:
email: tech@sanchezantonio.com

twitter: [@tsptoni](http://twitter.com/tsptoni)