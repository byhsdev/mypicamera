__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2014, MyPICamera"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "tech@sanchezantonio.com"
__status__ = "Production"

import requests

VARS = {'TEMP': 1, # ID for temperature in C
        'LUX': 3,  # ID for lux
        'HUM': 5,  # ID for humidity in %
        }

url = 'http://localhost:8083/JS/Run/zway.devices[2].instances[0].commandClasses[49].data[{0}].val.value'

def get_data():

    response = {}

    for key, value in VARS.iteritems():
        r = requests.get(url.format(value), timeout=1)
        response[key] = r.text

    return response

if __name__ == "__main__":
    print get_data()
