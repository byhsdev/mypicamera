#!/usr/bin/env python

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2014, MyPICamera"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "tech@sanchezantonio.com"
__status__ = "Production"

import tweepy
import simplejson
import settings
import camera
import environment

consumer_key = settings.CONSUMER_KEY
consumer_secret = settings.CONSUMER_SECRET
access_token = settings.ACCESS_TOKEN
access_token_secret = settings.ACCESS_TOKEN_SECRET

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

class StdOutListener(tweepy.streaming.StreamListener):
    """ A listener handles tweets are the received from the stream. """
    def on_data(self, data):
        try: 
            clean_data = simplejson.loads(data)
            text = clean_data['text']
            who = clean_data['user']['screen_name']
            status_id = clean_data['id']

            if who == 'MyPICamera':
                return True

            photo = camera.take_photo()
            print photo
            if photo:
                env_data = environment.get_data()
                temp = round(float(env_data['TEMP']), 2)
                hum = env_data['HUM']
                status = '@{0}, I see this! And temp is {1} degrees and {2}% humidity. #FSLMX2014 #CPMX5'.format(who, temp, hum)
                api.update_with_media(photo, status=status)
            else:
                status = '@{0}, Sorry, I couldn\'t take the photo :('.format(who)
                api.update_status(status=status)
        except Exception as e:
            print e
        
        return True

    def on_error(self, status):
        print status

if __name__ == "__main__":
    l = StdOutListener()

    stream = tweepy.Stream(auth, l)
    stream.filter(follow=['2549884932'], track=['@MyPICamera'])

