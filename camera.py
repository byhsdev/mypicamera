import time
import picamera
from datetime import datetime
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

def take_photo():
    photo = None
    with picamera.PiCamera() as camera:
        camera.resolution = (1024, 768)
        camera.start_preview()
        time.sleep(2)
        now = datetime.now().strftime('%Y%m%d-%H%M%S')
        name = '{0}.jpg'.format(now)
        photo = os.path.join(BASE_DIR, 'mypicamera/photos', name)
        camera.capture(photo)
        return photo

if __name__ == "__main__":
    take_photo()
